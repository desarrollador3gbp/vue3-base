export function GenerateUID() {
	return `${new Date().getTime()}${Math.random().toString(16).substr(2)}`;
}