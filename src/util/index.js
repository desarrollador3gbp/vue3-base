import { Storage } from './storage-format';
import { DateFormat } from './date-format';
import { Sentence } from './ucfirst';
import { FileToBase64 } from './file-to-base64';
import { UrlToFile } from './url-to-file';
import { GenerateUID } from './generate-uid';
import { DefaultImage } from './default-image';
import { GetFromLocalStorage, SaveToLocalStorage, RemoveFromLocalStorage } from './local-storage';

export {
	Storage,
	DateFormat,
	FileToBase64,
	Sentence,
	UrlToFile,
	GetFromLocalStorage,
	SaveToLocalStorage,
	RemoveFromLocalStorage,
	GenerateUID,
	DefaultImage
};