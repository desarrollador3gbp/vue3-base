export async function UrlToFile(url, filename, mimeType){
	const data = await fetch(url);
	const ArrayBuffer = await data.arrayBuffer();
	const file = new File([ArrayBuffer], filename,{type:mimeType});
	return file;
}