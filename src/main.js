import { createApp } from 'vue'
import './style.css'
import './plugins';
import App from './App.vue'

createApp(App).mount('#app')
